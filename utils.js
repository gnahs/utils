const elementPosition = (element) => {
    const rect = element.getBoundingClientRect()
    return rect
}

const createRandomId = () => {
    return Math.floor((Math.random() * 1000000) + 1)
}

const isMobile = () => {
    return (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    )
}

const isNodeChild = (el, tag, stop) => {
    if (el.getAttribute(tag)) {
        return el
    }
    while (el.parentNode) {
        if (el.classList.contains(stop)) {
            return null
        }
        // eslint-disable-next-line
        el = el.parentNode
        if (el.getAttribute(tag)) {
            return el
        }
    }

    return el
}

const h = (nodeName, attrs, ...children) => {
    const $el = document.createElement(nodeName)
    // eslint-disable-next-line
    for (const key in attrs) {
        $el.setAttribute(key, attrs[key])
    }
    children.forEach((child) => {
        if (typeof child === 'string') {
            $el.appendChild(document.createTextNode(child))
            return
        }
        $el.appendChild(child)
    })

    return $el
}

const clone = (object) => {
    const string =  JSON.stringify(object)
    return JSON.parse(string)
}

export {
    elementPosition,
    createRandomId,
    isMobile,
    isNodeChild,
    h,
    clone
}
